package entity

import org.scalatest._
import creature._


class EntitySpec extends FlatSpec {
  val origin = (0d, 0d)
  
  val cow1 = Cow(origin, 100, "Buryonka")
  val cow2 = Cow(origin, 150, "Milka")
  val cow3 = Cow(origin, 100, "Buryonka")

  "Different Cow instances" should "have different ID" in {
    assert(cow1.id !== cow3.id)
    assert(cow1.id !== cow2.id)
  }
  "Different Cow instances with the same parameters" should "be equal" in {
    assert(cow1 === cow3)
  }
}
