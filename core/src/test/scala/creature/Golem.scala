package entity
package creature

import feature._

case class Golem(position: (Double, Double), health: Double, mana: Double)
  extends Entity with Living with Magic { type This = Golem

  def moveTo(newPos: (Double, Double)): This = copy(position = newPos)
  def withHealth(newHealth: Double): This = copy(health = newHealth)
  def withMana(newMana: Double): This = copy(mana = newMana)
}
