package entity
package creature

import feature._

case class Ninja(position: (Double, Double), health: Double)
  extends Entity with Living { type This = Ninja

  def moveTo(newPos: (Double, Double)): This = copy(position = newPos)
  def withHealth(newHealth: Double): This = copy(health = newHealth)
}
