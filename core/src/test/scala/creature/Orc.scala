package entity
package creature

import feature._

case class Orc(position: (Double, Double), health: Double, name: String)
  extends Entity with Living with HasName { type This = Orc

  def moveTo(newPos: (Double, Double)): This = copy(position = newPos)
  def withHealth(newHealth: Double): This = copy(health = newHealth)
}
