package entity
package creature

import feature._

case class Human(position: (Double, Double), health: Double, name: String)
  extends Entity with Living with HasName { type This = Human

  def moveTo(newPos: (Double, Double)): This = copy(position = newPos)
  def withHealth(newHealth: Double): This = copy(health = newHealth)
}
