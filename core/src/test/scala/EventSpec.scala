package entity

import org.scalatest._

import creature._
import event._


class EventSpec extends FlatSpec {

  val origin = (0d, 0d)


  val john = Human(origin, 100.0, "John")
  val michael = Human(origin, 100.0, "Michael")
  val orc = Orc((0.777, 0.777), 1000, "Nzhur")
  val golem = Golem(origin, 32.5, 100)

  "Michael" should "have 40.0 remaining health after taking 60.0 damage" in {
    val damage = TakeDamage(john, michael, 60.0)
    info(s"Before: $michael")
    val newMichael = damage.applyTo(michael)
    info(s"After: $newMichael")
    assert(newMichael.health === 40.0)
  }

  "Time spaced Events" should "have different timestamps" in {
    val sleepTimeMs = 50
    val ev1 = TakeDamage(john, michael, 30d)
    info(s"First event timestamp: ${ev1.timestamp}")
    info(s"Sleeping for $sleepTimeMs ms")
    Thread.sleep(sleepTimeMs)
    val ev2 = TakeDamage(orc, golem, 40d)
    info(s"Second event timestamp: ${ev2.timestamp}")
    val timeDiff = ev2.timestamp - ev1.timestamp
    info(s"Time difference: $timeDiff ms")
    assert(ev1.timestamp !== ev2.timestamp)
    assert(timeDiff >= sleepTimeMs)
  }
}
