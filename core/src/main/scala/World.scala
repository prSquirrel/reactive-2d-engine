package com.alexey.game

import monifu.reactive.Observable
import monifu.concurrent.Scheduler

import scala.collection.concurrent.TrieMap
import model._

//TODO: make World regular class, so it can be extended
//and encapsulate all entity-related observables inside it
case class World[+T <: Entity](entities: Observable[(Long, T)]) {
  private[this] val state = TrieMap.empty[Long, T]
  private[this] val callback = entities.doWork(state += _)
  
  def startSimulation(implicit s: Scheduler) = callback.subscribe
  def readOnlySnapshot = state.readOnlySnapshot()
}
