package com.alexey.game
package screen

import com.badlogic.gdx._
import com.badlogic.gdx.graphics._
import com.badlogic.gdx.graphics.g2d._
//import com.badlogic.gdx.graphics.glutils._
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.Game
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.glutils.PixmapTextureData;
import com.badlogic.gdx.utils.viewport.Viewport
import com.badlogic.gdx.utils.viewport.FitViewport
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.Input.Keys



import scala.concurrent.duration._
import monifu.concurrent.atomic._
import monifu.reactive._
//import monifu.reactive.channels._
import monifu.concurrent.Implicits.globalScheduler
// import monifu.concurrent.Scheduler
// import scala.concurrent.ExecutionContext
// import java.util.concurrent.Executor
import controller._
import model._
import model.DynamicEntity._

class BouncingBalls(gameInstance: MyGame) extends Screen {
  private val width = Gdx.graphics.getWidth
  private val height = Gdx.graphics.getHeight
  // private val camera = new OrthographicCamera(width, height)
  //camera.position.set(width/2, height/2, 0)
  //camera.update()
  private val viewport = new FitViewport(width, height)
  private val batch = new SpriteBatch

  val obs = new InputObservable(bufferSize = 50)
  Gdx.input.setInputProcessor(obs)
  val in = obs.publish.refCount
  val interval = 1.second / 60

  val poller = InputPoller(in, 200.millis)
  poller.foreach(println)

  val clock = TickProducer(interval, 10.0).publish.refCount

  val radius = 4
  val n = 5000
  private val ballSprite = new Sprite(BouncingBalls.circle(radius))
  val world = BallWorld(n, radius)(clock, width, height)
  world.startSimulation

  def render(delta: Float) = {
    Gdx.gl.glClearColor(0.85f, 0.85f, 0.85f, 1)
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT)
    //Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA)
    //Gdx.gl.glActiveTexture(GL20.GL_TEXTURE0)
    batch.begin()
    world.readOnlySnapshot.foreach { e =>
      batch.draw(ballSprite, e._2.phys.position.x.toFloat, e._2.phys.position.y.toFloat)
    }
    batch.end()
    //println(world.readOnlySnapshot.getOrElse(0, "Woo"))
  }

  def resize(width: Int, height: Int) = {
    viewport.update(width, height)
  }
  def show() = {}
  def hide() = {}
  def pause() = {}
  def resume() = {}
  def dispose() = {
    batch.dispose()
  }
}

object BouncingBalls {
  //texture is flipped after creation from pixmap!!
  def circle(radius: Int): Texture = {
    val size = MathUtils.nextPowerOfTwo(radius * 2)
    val half = size / 2
    val pixmap = new Pixmap(size, size, Format.RGBA8888)
      pixmap.setColor(0.5f, 0, 0.25f, 1f)
      //pixmap.drawRectangle(0, 0, size, size)
      //                          height - y - 1
      pixmap.fillCircle(radius, size - radius - 1, radius - 1)
    val tex = new Texture(pixmap)
    pixmap.dispose()
    tex
  }
}
