package com.alexey.game

import monifu.reactive.Observable
import monifu.concurrent.Scheduler

import scala.collection.concurrent.TrieMap
import model._
import model.DynamicEntity._
import controller._

import com.softwaremill.quicklens._



object BallWorld {
  import model.PhysicsEvent._
  import model.EntityData._
  val rng = scala.util.Random

  def a(mass: Double, g: Double, k: Double): AccelerationFn[Double] = {
    def force(x: Position[Double], v: Velocity[Double], t: Double) = {
      val drag = k * v.length
      val grav = mass * g
      Vec2(-drag * v.x, -grav - drag * v.y) / mass
    }
    force _
  }
  def apply(n: Int, radius: Int)(clock: Observable[Tick], width: Int, height: Int): World[Ball] = {
    val min = 0.0
    val max = 100.0
    val rnorm = (radius - min)/(max - min)
    val k = 0.5 * 0.47 * 1.225 * math.Pi * rnorm * rnorm
    val initBalls = List.fill(n)(
      Ball(
        PhysicsData(
          Point2[Double](rng.nextInt(700), rng.nextInt(400)),
          velocity = Vec2[Double](rng.nextInt(1000), rng.nextInt(1000)),
          accelerationFn = a(0.5, 9.81, k)
        ),
        radius
      )
    )

    val ballsObs = initBalls.map { b =>
      val id = IdGenerator.next()
      val d = b.radius * 2
      val ebb = Vec2[Double](d, d)
      val wbb = Vec2[Double](width, height)
      val physics = PhysicsObservable(b.phys, ebb, wbb, clock)
      physics.map { vecs =>
        (id, b.modify(_.phys).setTo(vecs))
      }
    }

    val all = Observable.merge(ballsObs: _*)
    World(all)
  }
}
