package worksheet.reactivephysics.benchmark

import com.alexey.game.Vec2
import scala.annotation.tailrec


object Benchmark {// extends App {
  def acceleration(x: Double, v: Double, t: Double) = 2.5;
  def svacceleration(x: (Double, Double), v: (Double, Double), t: Double) = (2.5, 2.5);
  def vacceleration(x: Vec2[Double], v: Vec2[Double], t: Double) = Vec2(2.5, 2.5);

  def benchInteg[R](n: Int)(block: => R): Double = {
    @tailrec
    def loop(n: Int, t: Double): Double = {
      n match {
        case 0 => t
        case _ =>
          val t0 = System.nanoTime()
          block
          val t1 = System.nanoTime()
          loop(n - 1, t + (t1 - t0))
      }
    }
    val total = loop(n, 0L)
    val avgTime = total/n
    avgTime/1e6
  }
  def time[R](block: => R): R = {
    val t0 = System.nanoTime()
    val result = block    // call-by-name
    val t1 = System.nanoTime()
    println("Elapsed time: " + (t1 - t0)/1e6 + "ms")
    result
  }



  val runs = 50000
  val first = benchInteg(runs) {
    Integrator.dvv(acceleration, 5000, 0.1)(0.0, 10.0, 0.0)
  }
  val second = benchInteg(runs) {
    VectorIntegrator.dvv(vacceleration, 5000, 0.1)(Vec2(0.0, 0.0), Vec2(10.0, 10.0), 0.0)
  }
  val third = benchInteg(runs) {
    SemiVectorIntegrator.dvv(svacceleration, 5000, 0.1)((0.0, 0.0), (10.0, 10.0), 0.0)
  }
  println(s"[Regular]: ${first} ms over ${runs} runs.")
  println(s"[Vector ]: ${second} ms over ${runs} runs.")
  println(s"[Tuple  ]: ${third} ms over ${runs} runs.")
  println(s"[Vector] is slower by x${second/first}")
}
