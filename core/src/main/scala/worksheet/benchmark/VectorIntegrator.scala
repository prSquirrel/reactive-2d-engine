package worksheet.reactivephysics.benchmark

import scala.annotation.tailrec

import com.alexey.game.Vec2
import spire.syntax.fractional._
import spire.math.Fractional
import scala.{specialized => sp}
import spire.syntax.literals._
// trait SpecFunc3[@sp(Float, Double) -T1,
//                 @sp(Float, Double) -T2,
//                 @sp(Float, Double) -T3,
//                 @sp(Float, Double)  +R] extends AnyRef {
//   def apply(v1: T1, v2: T2, v3: T3): R
// }
// val my = new SpecFunc3[Vec2[Double], Vec2[Double], Double, Vec2[Double]] {
//   def apply(x: Vec2[Double], v: Vec2[Double], t: Double): Vec2[Double] = {
//     throw new IllegalArgumentException("tttttt")
//     Vec2(2.5, 2.5)
//   }
// }
object VectorIntegrator {
  // def f[@sp(Float, Double) T: Fractional](x: Vec2[T], v: Vec2[T], t: T): Vec2[T] = {
  //   //throw new IllegalArgumentException("dsadsadasdas")
  //   val a = Fractional[T].fromDouble(2.5)
  //   Vec2(a, a)
  // }
  def dampedVelocityVerlet[@sp(Float, Double) T: Fractional](
    f: (Vec2[T], Vec2[T], T) => Vec2[T],
    delta: T)(
    r: Vec2[T],
    v: Vec2[T],
    t: T): (Vec2[T], Vec2[T]) = {
      val halfDelta = 0.5 * delta

      val a = f(r, v, t)
      val rNew = r + v * delta + a * (halfDelta*delta)
      val aNew = a + f(rNew, v + a*delta, t + delta)
      val vNew = v + aNew * halfDelta

      val aRefined = a + f(rNew, vNew, t + delta)
      val vRefined = v + aRefined * halfDelta
      (rNew, vRefined)
  }

  @tailrec
  def dvv[@sp(Float, Double) T: Fractional](
    f: (Vec2[T], Vec2[T], T) => Vec2[T],
    n: Int,
    delta: T)(
      r0: Vec2[T], v0: Vec2[T], t0: T): (Vec2[T], Vec2[T]) = {
        n match {
          case 0 => (r0, v0)
          case _ =>
            val (r1, v1) = dampedVelocityVerlet(f, delta)(r0, v0, t0)
            dvv(f, n - 1, delta)(r1, v1, t0 + delta)
        }
    }
}
