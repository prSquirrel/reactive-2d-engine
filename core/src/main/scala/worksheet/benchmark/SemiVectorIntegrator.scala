package worksheet.reactivephysics.benchmark

import scala.math._
import scala.annotation.tailrec


object SemiVectorIntegrator {
  def dampedVelocityVerlet(
    f: ((Double, Double), (Double, Double), Double) => (Double, Double),
    delta: Double)(
    r: (Double, Double),
    v: (Double, Double),
    t: Double): ((Double, Double), (Double, Double)) = {

      val half = 0.5

      val a = f(r, v, t)
      val rNew = (r._1 + v._1 * delta + a._1 * (half*delta*delta), r._2 + v._2 * delta + a._2 * (half*delta*delta))
      val aSemi = (v._1 + a._1*delta, v._2 + a._2*delta)
      val aNew = (a._1 + f(rNew, aSemi, t + delta)._1, a._2 + f(rNew, aSemi, t + delta)._2)
      val vNew = (v._1 + aNew._1 * half*delta, v._2 + aNew._2 * half*delta)
      val aRefined = (a._1 + f(rNew, vNew, t + delta)._1, a._2 + f(rNew, vNew, t + delta)._2)
      val vRefined = (v._1 + aRefined._1 * half*delta, v._2 + aRefined._2 * half*delta)
      (rNew, vRefined)
  }

  @tailrec
  def dvv(
    f: ((Double, Double), (Double, Double), Double) => (Double, Double),
    n: Int,
    delta: Double)(
      r0: (Double, Double), v0: (Double, Double), t0:Double): ((Double, Double), (Double, Double)) = {
        n match {
          case 0 => (r0, v0)
          case _ =>
            val (r1, v1) = dampedVelocityVerlet(f, delta)(r0, v0, t0)
            dvv(f, n - 1, delta)(r1, v1, t0 + delta)
        }
    }
}
