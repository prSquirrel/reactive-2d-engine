package worksheet.reactivephysics.benchmark

import scala.math._
import scala.annotation.tailrec

object Integrator {
  def rk4Iter(
    f: (Double, Double) => Double,
    t: Double,
    y: Double,
    h: Double) = {
    val k1 = h * f(t        , y)
    val k2 = h * f(t + 0.5*h, y + 0.5*k1)
    val k3 = h * f(t + 0.5*h, y + 0.5*k2)
    val k4 = h * f(t + h    , y + k3)

    y + (k1 + 2*(k2 + k3) + k4) / 6
  }
  @tailrec
  def rk4(
    f: (Double, Double) => Double,
    t0: Double,
    y0: Double,
    n: Int,
    h: Double
  ): Double = {
    n match {
      case 0 => y0
      case _ =>
        val y1 = rk4Iter(f, t0, y0, h)
        rk4(f, t0 + h, y1, n - 1, h)
    }
  }
  def dampedVelocityVerlet(
    f: (Double, Double, Double) => Double,
    h: Double
  )(x: Double, v: Double, t: Double) = {
    val a = f(x, v, t)
    val xNew     = x + h * v + 0.5*h*h * a
    val vNew     = v + 0.5*h * (a + f(xNew, v + h*a, t + h))
    val vRefined = v + 0.5*h * (a + f(xNew, vNew, t + h))
    (xNew, vRefined)
  }
  @tailrec
  def dvv(
    f: (Double, Double, Double) => Double,
    n: Int,
    h: Double)(x0: Double, v0: Double, t0:Double): (Double, Double) = {
      //println(f"[dt=${h}%2.2f][time:${t0}%2.2f]: (${x0}%2.2f, ${v0}%2.2f)")
    n match {
      case 0 => (x0, v0)
      case _ =>
        val (x1, v1) = dampedVelocityVerlet(f, h)(x0, v0, t0)
        dvv(f, n - 1, h)(x1, v1, t0 + h)
    }
  }

  def velocityVerlet(
    f: (Double, Double) => Double,
    h: Double)(
      t: Double,
      r: Double,
      v: Double
    ) = {
    val rNew = r + h * v + (0.5*h*h) * f(t, r)
    val vNew = v + 0.5*h * (f(t, r) + f(t + h, rNew))
    (rNew, vNew)
  }
  @tailrec
  def vv(
    f: (Double, Double) => Double,
    t0: Double,
    r0: Double,
    v0: Double,
    n: Int,
    h: Double): (Double, Double) = {
    n match {
      case 0 => (r0, v0)
      case _ =>
        val (r1, v1) = velocityVerlet(f, h)(t0, r0, v0)
        vv(f, t0 + h, r1, v1, n - 1, h)
    }
  }
}
