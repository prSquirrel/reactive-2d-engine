package worksheet.components

import shapeless._
import poly._


sealed trait Component
case class Position(x: Float, y: Float) extends Component
case class Velocity(x: Float, y: Float) extends Component
case class HP(health: Float) extends Component
case class Attributes(strength: Int, dexterity: Int, agility: Int) extends Component

sealed trait Entity[C <: HList] {
  final val id: Long = IdGenerator.next()
  def components: C
}

case class Human(components: Position :: Velocity :: HNil) extends Entity[Position :: Velocity :: HNil]

object Test {// extends App {
  //val h = Human((Position(0f,0f), Velocity(0f,0f)))
}



object IdGenerator {
  private val counter = new Iterator[Long] {
    var i = -1
    def hasNext = true
    def next() = { i += 1; i }
  }

  def next(): Long = counter.next()
}
