package worksheet.reactivephysics

import monifu.reactive._
import scala.concurrent.duration._
import monifu.reactive.observables._
import monifu.concurrent._
import scala.math._
import scala.annotation.tailrec

import scalax.chart.api._
import scalax.chart._

import org.jfree.chart.plot._
import org.jfree.chart.title.TextTitle
import com.alexey.game.Point2
import com.alexey.game.Vec2
import com.alexey.game.Integrator
import com.alexey.game.model.EntityData._

object ReactivePhysics extends App {
  def toList[T](o: Observable[T])(implicit s: Scheduler) = {
      o.foldLeft(Vector.empty[T])(_ :+ _).asFuture
        .map(_.getOrElse(Vector.empty))
  }
  def a(mass: Double, g: Double, k: Double): AccelerationFn[Double] = {
    def force(x: Position[Double], v: Velocity[Double], t: Double) = {
      val drag = k * v.length
      val grav = mass * g
      Vec2(-drag * v.x, -grav - drag * v.y) / mass
    }
    force _
  }


  import monifu.concurrent.Implicits.globalScheduler


  val period = 1.second / 60
  val dt = period.toUnit(SECONDS)
  //time function in seconds: Long
  val time = Observable
    .intervalAtFixedRate(period)
    .map(_ * dt)

  val r0 = Point2(0.0, 0.0)
  val v0 = Vec2(10.0, 100.0)
  val p = time.scan(r0, v0) { (state, t) =>
    Integrator.dampedVelocityVerlet(a(1.0, 9.81, 0.287875 * 0.007854), dt)(state._1, state._2, t)
  }


  val timeLimit = 30.seconds.toUnit(SECONDS)
  val pts = time.zip(p).takeWhile {
    case (t, st) => t < timeLimit
  }



  val xVelSeries = new XYSeries("x")
  val yVelSeries = new XYSeries("y")
  val velChart = XYLineChart(
    Seq(xVelSeries, yVelSeries),
    title = "Velocity"
  )
  velChart.plot.domain.axis.label.text = "t, s"
  velChart.plot.range.axis.label.text = "v(t), m/s"
  val velocity = pts doWork {
    case (t, st) =>
      xVelSeries.add(t, st._2.x)
      yVelSeries.add(t, st._2.y)
  }
  velChart.show()



  val xPosSeries = new XYSeries("x")
  val yPosSeries = new XYSeries("y")
  val posChart = XYLineChart(
    Seq(xPosSeries, yPosSeries),
    title = "Position"
  )
  posChart.plot.domain.axis.label.text = "t, s"
  posChart.plot.range.axis.label.text = "x(t), m"
  val position = pts doWork {
    case (t, st) =>
      xPosSeries.add(t, st._1.x)
      yPosSeries.add(t, st._1.y)
  }
  posChart.show()



  val pointSeries = new XYSeries("material point")
  val movChart = XYLineChart(
    pointSeries,
    title = "XY movement trace"
  )
  movChart.plot.domain.axis.label.text = "x, m"
  movChart.plot.range.axis.label.text = "y, m"
  val movement = pts doWork {
    case (t, st) =>
      pointSeries.add(st._1.x, st._1.y)
  }
  movChart.show()


  movement.subscribe()
  position.subscribe()
  velocity.subscribe()
}
// //sphere radius in meters
// val radius = 0.5
// //velocity in meters per second
// val v0 = 5
// val g = 9.81
// //drag coefficient for sphere
// val dragC = 0.47
// //mass in kg
// val mass = 0.05
// //cross-sectional area
// val area = Pi * radius * radius
// //air density in kg/m^3
// val density = 1.225
// val a = 20
