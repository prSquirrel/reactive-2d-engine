package com.alexey.game


object IdGenerator {
  private val counter = new Iterator[Long] {
    var i = -1
    def hasNext = true
    def next() = { i += 1; i }
  }

  def next(): Long = counter.next()
}
