package com.alexey.game.model


sealed trait State

object State {
  case object Run extends State
  case object Pause extends State
}
