package com.alexey.game.model

import EntityData._


sealed trait DynamicEntity extends Entity

object DynamicEntity {
  case class Ball(phys: PhysicsData, radius: Int) extends DynamicEntity
}
