package com.alexey.game.model


sealed trait StaticEntity extends Entity

sealed trait Border extends StaticEntity

object StaticEntity {
  object Border {
    case object Top extends Border
    case object Bottom extends Border
    case object Left extends Border
    case object Right extends Border
  }
}
