package com.alexey.game.model


sealed trait InputEvent extends Event

object InputEvent {
  case class KeyDown(code: Int) extends InputEvent
  case class KeyUp(code: Int) extends InputEvent
}
