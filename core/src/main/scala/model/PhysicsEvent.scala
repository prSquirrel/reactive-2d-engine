package com.alexey.game.model


sealed trait PhysicsEvent extends Event

object PhysicsEvent {
  case class Tick(total: Double, delta: Double) extends PhysicsEvent
}
