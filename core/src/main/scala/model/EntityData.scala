package com.alexey.game
package model


sealed trait EntityData


object EntityData {
  type Position[T] = Point2[T]
  type Velocity[T] = Vec2[T]
  type Acceleration[T] = Vec2[T]
  type AccelerationFn[T] = (Position[T], Velocity[T], T) => Acceleration[T]

  case class PhysicsData(
    position:     Position[Double],
    velocity:     Velocity[Double],
    accelerationFn: AccelerationFn[Double]
  ) extends EntityData
}
