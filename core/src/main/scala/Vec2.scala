package com.alexey.game

import spire.syntax.fractional._
import spire.math.Fractional
import spire.algebra.Signed
import spire.syntax.signed._
import scala.{specialized => sp}


case class Vec2[@sp(Float, Double) T: Fractional](x: T, y: T) {
  def +(v: Vec2[T]) = Vec2(x + v.x, y + v.y)
  def -(v: Vec2[T]) = Vec2(x - v.x, y - v.y)
  def *(v: Vec2[T]) = Vec2(x * v.x, y * v.y)
  def /(v: Vec2[T]) = Vec2(x / v.x, y / v.y)

  def squaredLength: T = (x * x) + (y * y)
  def length: T = squaredLength.sqrt
  def normalized: Vec2[T] = Vec2(x / length, y / length)

  def direction: Vec2[T] = {
    val nx = Fractional[T].fromInt(Signed[T].signum(x))
    val ny = Fractional[T].fromInt(Signed[T].signum(y))
    Vec2(nx, ny)
  }

  def *(s: T) = Vec2(x * s, y * s)
  def /(s: T) = Vec2(x / s, y / s)
  def sum: T = x + y
  def dot(v: Vec2[T]): T = x * v.x + y * v.y
  def cross(v: Vec2[T]): T = x * v.y - y * v.x
  def unary_-(): Vec2[T] = Vec2(-x, -y)
  def reverseX: Vec2[T] = Vec2(-x, y)
  def reverseY: Vec2[T] = Vec2(x, -y)
}
