package com.alexey.game

import com.badlogic.gdx.Game

import screen._


class MyGame extends Game {
  override def create() {
    setScreen(new BouncingBalls(this))
  }
}
