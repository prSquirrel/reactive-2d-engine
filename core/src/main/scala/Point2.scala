package com.alexey.game

import spire.syntax.fractional._
import spire.math.Fractional
import scala.{specialized => sp}


case class Point2[@sp(Float, Double) T: Fractional](x: T, y: T) {
  def +(p: Point2[T]) = Point2(x + p.x, y + p.y)
  def +(v: Vec2[T]) = Point2(x + v.x, y + v.y)
  def -(p: Point2[T]) = Point2(x - p.x, y - p.y)
  def -(v: Vec2[T]) = Point2(x - v.x, y - v.y)

  def unary_-(): Point2[T] = Point2(-x, -y)
}
