package com.alexey.game
package controller

import scala.concurrent.duration._

import monifu.reactive._

import model._
import model.PhysicsEvent._
import model.EntityData._
import model.StaticEntity.Border._

import com.softwaremill.quicklens._


object PhysicsObservable {
  def apply(
    initData: PhysicsData,
    entityBB: Vec2[Double],
    worldBB:  Vec2[Double],
    clock:    Observable[Tick]): Observable[PhysicsData] = {

    val state = clock.scan(initData) { (vecs, t) =>
      val next = dvv(vecs, t)
      val c = wallCollision(entityBB, worldBB)(next.position)
      val rot = rotation(c)
      dvv(vecs.modify(_.velocity).using(rot), t)
    }
    state
  }

  def dvv(phys: PhysicsData, t: Tick): PhysicsData = {
    val (pos, vel) = Integrator.dampedVelocityVerlet(phys.accelerationFn, t.delta)(phys.position, phys.velocity, t.total)
    phys
      .modify(_.position).setTo(pos)
      .modify(_.velocity).setTo(vel)
  }

  def wallCollision(ebb: Vec2[Double], wbb: Vec2[Double])(pos: Position[Double]): Option[Border] = {
    pos match {
      case Point2(x, _) if  x <= 0f                => Some(Left)
      case Point2(x, _) if (x + ebb.x) >= wbb.x    => Some(Right)
      case Point2(_, y) if  y <= 0f                => Some(Bottom)
      case Point2(_, y) if (y + ebb.y) >= wbb.y    => Some(Top)
      case Point2(_, _)                            => None
    }
  }

  type VelocityRotation = Vec2[Double] => Vec2[Double]
  def rotation(c: Option[Border]): VelocityRotation = c match {
    case Some(b) =>
      b match {
        case Left => _.reverseX
        case Right => _.reverseX
        case Bottom => _.reverseY
        case Top => _.reverseY
      }
    case None => identity(_)
  }
}
