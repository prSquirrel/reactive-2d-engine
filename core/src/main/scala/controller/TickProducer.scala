package com.alexey.game
package controller

import scala.concurrent.duration._

import monifu.reactive._
import monifu.reactive.observables._

import model.PhysicsEvent._


object TickProducer {
  def apply(period: FiniteDuration, speedMultiplier: Double): Observable[Tick] = {
    val delta: Double = period.toUnit(SECONDS) * speedMultiplier
    val clock = Observable
      .intervalAtFixedRate(period)
      .map(l => Tick(delta * l, delta))
    clock
  }
}
