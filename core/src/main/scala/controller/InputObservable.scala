package com.alexey.game
package controller

import com.badlogic.gdx.InputAdapter
import com.badlogic.gdx.Input.Keys

import monifu.reactive._
import monifu.reactive.channels._
import monifu.concurrent.Implicits.globalScheduler

import model.InputEvent
import model.InputEvent._



final class InputObservable(bufferSize: Int) extends InputAdapter with Observable[InputEvent] {
  private val channel = PublishChannel[InputEvent](OverflowStrategy.DropOld(bufferSize = bufferSize))

  def onSubscribe(sub: Subscriber[InputEvent]): Unit = {
    channel.onSubscribe(sub)
  }

  override def keyUp(keycode: Int): Boolean = {
    channel.pushNext(KeyUp(keycode))
    true
  }
  override def keyDown(keycode: Int): Boolean = {
    channel.pushNext(KeyDown(keycode))
    true
  }
}
