package com.alexey.game
package controller


import monifu.reactive._
import monifu.reactive.observables._

import scala.concurrent.duration._

import com.badlogic.gdx.Input.Keys

import model.InputEvent
import model.InputEvent._


object InputPoller {
  def apply(input: Observable[InputEvent], pollingRate: FiniteDuration): Observable[Set[KeyDown]] = {
    val keysHeld = input
      //.filter(isKeyEvent(_))
      .scan(Set.empty[KeyDown]) { (state, key) =>
        key match {
          case KeyDown(code) => state + KeyDown(code)
          case KeyUp(code) => state - KeyDown(code)
          case _ => state
        }
      }
    val repeater = keysHeld.echoRepeated(pollingRate)
    repeater
  }

  def handleKey(key: KeyDown): Vec2[Double] = {
    key.code match {
      case Keys.W => Vec2(0, 1)
      case Keys.D => Vec2(1, 0)
      case Keys.A => Vec2(-1, 0)
      case Keys.S => Vec2(0, -1)
      case      _ => Vec2(0, 0)
    }
  }

  def handleKeys(fromKeys: Set[KeyDown], magnitude: Double): Vec2[Double] = {
    val func = (d: Vec2[Double], k: KeyDown) => d + handleKey(k)
    val sum = fromKeys.foldLeft(Vec2(0d, 0d))(func)
    sum * magnitude
  }
}
