package com.alexey.game

import spire.syntax.fractional._
import spire.math.Fractional
import spire.syntax.literals._

import scala.annotation.tailrec
import scala.{specialized => sp}


object Integrator {
  import model.EntityData._

  def dampedVelocityVerlet[@sp(Float, Double) T: Fractional](
    f:     AccelerationFn[T],
    delta: T)(
    r:     Position[T],
    v:     Velocity[T],
    t:     T): (Position[T], Velocity[T]) = {
      val halfDelta = 0.5 * delta

      val a = f(r, v, t)
      val rNew = r + v * delta + a * (halfDelta*delta)
      val aNew = a + f(rNew, v + a*delta, t + delta)
      val vNew = v + aNew * halfDelta

      val aRefined = a + f(rNew, vNew, t + delta)
      val vRefined = v + aRefined * halfDelta
      (rNew, vRefined)
  }
  // @tailrec
  // def dvv[@sp(Float, Double) T: Fractional](
  //   f:     (Point2[T], Vec2[T], T) => Vec2[T],
  //   n:     Int,
  //   delta: T)(
  //   r0:    Point2[T],
  //   v0:    Vec2[T],
  //   t0:    T): (Point2[T], Vec2[T]) = {
  //       n match {
  //         case 0 => (r0, v0)
  //         case _ =>
  //           val (r1, v1) = dampedVelocityVerlet(f, delta)(r0, v0, t0)
  //           dvv(f, n - 1, delta)(r1, v1, t0 + delta)
  //       }
  //   }
}
