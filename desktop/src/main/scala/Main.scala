package com.alexey.game

import com.badlogic.gdx.backends.lwjgl._

object Main {
  def main(args: Array[String]): Unit = {
    val cfg = new LwjglApplicationConfiguration
    cfg.title = "reactive-game-engine"
    cfg.height = 480//800
    cfg.width = 800//1280
    cfg.forceExit = false
    //cfg.fullscreen = true

    // cfg.vSyncEnabled = false
    // cfg.foregroundFPS = 0
    // cfg.backgroundFPS = 0
    //System.setProperty("org.lwjgl.opengl.Window.undecorated", "true")
    new LwjglApplication(new MyGame(), cfg)
  }
}
